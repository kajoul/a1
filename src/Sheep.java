
public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      int goatCount = 0;

      for (int i = 0; i < animals.length; i++) {
         if(animals[i].equals(Animal.goat)){
            goatCount++;
         }
      }

      if (goatCount == 0 || animals.length == goatCount){
         return;
      }

      for (int i = 0; i < goatCount; i++) {
         animals[i] = Animal.goat;
      }

      for (int i = goatCount; i < animals.length; i++) {
         animals[i] = Animal.sheep;
      }
   }
}

